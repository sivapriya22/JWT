from flask_jwt_extended import JWTManager
from flask import Flask, request, jsonify
from flask_jwt_extended import create_access_token,jwt_required, get_jwt_identity
from models import Enduser,db



app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:Password!12@localhost/users'

db.init_app(app)
db.app = app

# Setup the Flask-JWT-Extended extension
app.config["JWT_SECRET_KEY"] = "super-secret"  # Change this "super secret" with something else!
jwt = JWTManager(app)


# @app.route("/")
# def hello():
#     return "Hello World!"

def create_tb():
    db.create_all()

@app.route("/token", methods=["POST"])
def create_token():
    username = request.json.get("username", None)
    password = request.json.get("password", None)
    # Query your database for username and password
    user = Enduser.query.filter_by(username=username, password=password).first()
    if user is None:
        # the user was not found on the database
        return jsonify({"msg": "Bad username or password"}), 401

    # create a new token with the user id inside
    access_token = create_access_token(identity=user.id)
    return jsonify({"token": access_token, "user_id": user.id})


@app.route("/protected", methods=["GET"])
@jwt_required()
def protected():
    # Access the identity of the current user with get_jwt_identity
    current_user_id = get_jwt_identity()
    #user = Enduser.query.filter.get(current_user_id)
    user = Enduser.query.filter_by(id=current_user_id).first()

    return jsonify({"id": user.id, "username": user.username}), 200


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    create_tb()
    app.run()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
